package com.example.test_project4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestProject4Application {

    public static void main(String[] args) {
        SpringApplication.run(TestProject4Application.class, args);
    }

}
